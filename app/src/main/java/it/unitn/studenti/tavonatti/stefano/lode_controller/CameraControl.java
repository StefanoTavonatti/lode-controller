package it.unitn.studenti.tavonatti.stefano.lode_controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.LodeBrodcastReceiver;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.NetworkService;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.Utilities.NetworkServiceConnection;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;

public class CameraControl extends AppCompatActivity {

    private NetworkServiceConnection serviceConnection=new NetworkServiceConnection();

    public final static String ARG_CANTILT="ARG_CANTILT";
    public final static String ARG_CANPAN="ARG_CANPAN";
    public final static String ARG_CANZOOM="ARG_CANZOOM";

    private boolean cantilt=false;
    private boolean canpan=false;
    private boolean canzoom=false;

    private class CameraContrellerBrodcastReceiver extends LodeBrodcastReceiver{

        @Override
        protected void onCanPan(boolean value){
            canpan=value;
            refreshButtons();
        }

        @Override
        protected void onCanTilt(boolean value){
            cantilt=value;
            refreshButtons();
        }

        @Override
        protected void onCanZoom(boolean value){
            canzoom=value;
            refreshButtons();
        }
    }

    private BroadcastReceiver receiver=new CameraContrellerBrodcastReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_control);

        Intent intent=getIntent();

        if(intent.hasExtra(ARG_CANPAN)){
            canpan=intent.getBooleanExtra(ARG_CANPAN,false);
        }

        if(intent.hasExtra(ARG_CANTILT)){
            cantilt=intent.getBooleanExtra(ARG_CANTILT,false);
        }

        if(intent.hasExtra(ARG_CANZOOM)){
            canzoom=intent.getBooleanExtra(ARG_CANZOOM,false);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//visualizzare il tasto indietro sull'actionbar
        getSupportActionBar().setHomeButtonEnabled(true);

        refreshButtons();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id==android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_camera_control,menu);
        return true;
    }

    private void refreshButtons() {
        Button up= (Button) findViewById(R.id.up_camera_control);
        Button down= (Button) findViewById(R.id.down_camera_control);
        Button left= (Button) findViewById(R.id.left_camera_control);
        Button right= (Button) findViewById(R.id.right_camera_control);
        Button zoomIn= (Button) findViewById(R.id.zoom_in_camera_control);
        Button zoomOut= (Button) findViewById(R.id.zoom_out_camera_control);

        up.setEnabled(cantilt);
        down.setEnabled(cantilt);
        left.setEnabled(canpan);
        right.setEnabled(canpan);
        zoomIn.setEnabled(canzoom);
        zoomOut.setEnabled(canzoom);
    }

    @Override
    protected void onResume(){
        super.onResume();
        Intent intent=new Intent(this,NetworkService.class);
        this.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, new IntentFilter(Constant.NOTIFICATION));//Brodcast locale, solo per la mia app

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

        if(settingsDBManager.readBool(Constant.KEEP_SCREEN_ACTIVE,Constant.DEFAULT_SCREEN_ON))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    @Override
    protected void onPause(){
        super.onPause();

        unbindService(serviceConnection);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
    }

    public void onClickUP(View view){
        //TODO aggiungere i comandi da inviare
    }

    public void onClickDown(View view){

    }

    public void onClickRight(View view){

    }

    public void onClickLeft(View view){

    }

    public void onClickZoomIn(View view){

    }

    public void onClickZoomOut(View view){

    }
}
