package it.unitn.studenti.tavonatti.stefano.lode_controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.LodeBrodcastReceiver;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.NetworkService;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.Utilities.NetworkServiceConnection;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.NetworkMessage;

public class SlideControlActivity extends AppCompatActivity {

    private NetworkServiceConnection serviceConnection=new NetworkServiceConnection();
    private boolean isSyncComplete=false;
    private BroadcastReceiver receiver=new SlideControlLodeBrodcastReceiver();

    private int activeSlide=-1;

    /**
     * estensione della classe per riabilitare il pulsante "slide browser" una volta terminata la sincronizzazione
     */
    private class SlideControlLodeBrodcastReceiver extends LodeBrodcastReceiver{//TODO continuare e cambiare nome

        @Override
        protected void onSyncComplete(){
            super.onSyncComplete();
            enableSlideVrowser();
        }

        @Override
        protected void onActiveSlide(){
            setActiveSlideTextView();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_control);

        if(this.getIntent().hasExtra(Constant.SYNC_COMPLETE)){
            isSyncComplete=getIntent().getBooleanExtra(Constant.SYNC_COMPLETE,false);
        }

        if(isSyncComplete){
            enableSlideVrowser();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//visualizzare il tasto indietro sull'actionbar
        getSupportActionBar().setHomeButtonEnabled(true);

        if(getIntent().hasExtra(Constant.ACTIVE_SLIDE)){
            activeSlide=getIntent().getIntExtra(Constant.ACTIVE_SLIDE,-1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_slide_control, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(this,SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id==android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onClickBack1(View view){
        finish();
    }

    @Override
    protected void onPause(){
        super.onPause();
        unbindService(serviceConnection);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
    }

    @Override
    protected void onResume(){
        super.onResume();
        Intent intent2=new Intent(this,NetworkService.class);
        this.bindService(intent2, serviceConnection, Context.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, new IntentFilter(Constant.NOTIFICATION));//Brodcast locale, solo per la mia app

        setActiveSlideTextView();

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

        if(settingsDBManager.readBool(Constant.KEEP_SCREEN_ACTIVE,Constant.DEFAULT_SCREEN_ON))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    private void enableSlideVrowser(){
        Button showSlideBrowser= (Button) findViewById(R.id.slide_control_show_slide_browser);
        showSlideBrowser.setEnabled(true);
        showSlideBrowser.setText(getString(R.string.slide_control_activity_show_slider_browser));
    }

    public void onClickSlideBrowser(View view){
        Intent intent=new Intent(this,SlideBrowserActivity.class);
        intent.putExtra(NetworkMessage.ACTIVE_SLIDE,serviceConnection.getService().getActiveSlide());
        intent.putExtra(NetworkMessage.SLIDES, serviceConnection.getService().slides);//TODO recuperare l'elenco delle slide nel browser //TODO l'elenco delle slide non esiste se è in corso la sincro
        startActivity(intent);
    }

    public void onClickPrevious(View view){
        serviceConnection.getService().sendCommandToServer(NetworkMessage.PREVIOUS);
    }

    public void onClickNext(View view){
        serviceConnection.getService().sendCommandToServer(NetworkMessage.NEXT);
    }

    private void setActiveSlideTextView(){

        if(serviceConnection.getService()!=null){
            activeSlide=serviceConnection.getService().getActiveSlide();
        }

        TextView textView= (TextView) findViewById(R.id.active_slide_slide_control);
        textView.setText(getString(R.string.current_slide)+"   "+ (activeSlide>-1 ?activeSlide:"--"));
    }
}
