package it.unitn.studenti.tavonatti.stefano.lode_controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.LodeBrodcastReceiver;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.NetworkService;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.Utilities.NetworkServiceConnection;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.NetworkMessage;

public class ChangeRecordingStateActivity extends AppCompatActivity {

    private NetworkServiceConnection serviceConnection=new NetworkServiceConnection();
    private BroadcastReceiver receiver=new LodeBrodcastReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_recording_state);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//visualizzare il tasto indietro sull'actionbar
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_recording_state, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(this,SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id==android.R.id.home){
            closeActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume(){
        super.onResume();
        Intent intent2=new Intent(this,NetworkService.class);
        this.bindService(intent2, serviceConnection, Context.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, new IntentFilter(Constant.NOTIFICATION));//Brodcast locale, solo per la mia app

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

        if(settingsDBManager.readBool(Constant.KEEP_SCREEN_ACTIVE,Constant.DEFAULT_SCREEN_ON))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onPause(){
        super.onPause();
        unbindService(serviceConnection);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
    }

    public void onClickStart(View view){
        serviceConnection.getService().sendCommandToServer(NetworkMessage.START);
    }

    public void onClickPause(View view){
        serviceConnection.getService().sendCommandToServer(NetworkMessage.PAUSE);
    }

    public void onClickStop(View view){
        serviceConnection.getService().sendCommandToServer(NetworkMessage.STOP);
    }

    public void onClickBack(View view){
        closeActivity();
    }

    @Override
    public void onBackPressed(){
        /* Override del tasto back*/
        closeActivity();
    }

    private  void closeActivity(){

        Intent intent=new Intent();
        String status=serviceConnection.getService().getStatus();

        intent.putExtra(Constant.STATUS,status);

        setResult(Constant.RESULT_OK,intent);

        finish();
    }
}
