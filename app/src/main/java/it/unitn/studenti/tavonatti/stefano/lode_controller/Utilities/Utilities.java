package it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.util.Hashtable;

/**
 * Created by stefano on 17/09/15.
 */
public class Utilities {

    public static boolean isMyServiceRunning(Class<?> serviceClass,Context context){
        ActivityManager manager= (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static Bitmap scaleBitmapFromFile(File file,int maxWidth,int maxHeight){

        //TODO contrtolli null e placeholder

        BitmapFactory.Options options =new BitmapFactory.Options();

        //l'immagine non viene efettivamente caricata, ma solo i dati riguardanti la sua grandezza
        options.inJustDecodeBounds=true;

        BitmapFactory.decodeFile(file.getAbsolutePath(),options);

        //calcolo di quante volte devo ridurre l'imagine
        options.inSampleSize=calculateInSampleSize(options,maxWidth,maxHeight);

        options.inJustDecodeBounds=false;

        Bitmap img=BitmapFactory.decodeFile(file.getAbsolutePath(),options);

        return img;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Prendo l'altezza e la larghezza della Bitmap
        final int height = options.outHeight; //1920x1200 nell'immagine di prova
        final int width = options.outWidth;
        int inSampleSize = 1;

        reqHeight /= 2;
        reqWidth /= 2;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calcolo il più grande inSampleSize (che è una potenza di 2) idoneo a comprimere l'immagine
            // in modo che sia entro le dimensioni richieste (reqHeight e reqWidth)
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }

            Log.d("W/H", halfWidth / inSampleSize + ":" + halfHeight / inSampleSize + "       " + inSampleSize);
        }

        return inSampleSize;
    }

    public static Hashtable<String, String> decodeMeassage(String message){
        Hashtable<String, String> result=new Hashtable<String,String>();

        String[] splits=message.split(" ");

        if(splits.length>=1){
            result.put(NetworkMessage.COMMAND_FIELD, splits[0]);

            if(splits.length>=2){
                result.put(NetworkMessage.VALUES_FIELD, splits[1]);
            }
        }

        return result;
    }
}
