package it.unitn.studenti.tavonatti.stefano.lode_controller.Network.AsyncTasks;

import android.os.AsyncTask;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.Utilities.NetworkServiceConnection;
import it.unitn.studenti.tavonatti.stefano.lode_controller.View1;

/**
 * Created by stefano on 24/08/15.
 */
public class ConnectionTask extends AsyncTask<NetworkServiceConnection,Void,Integer>{//TODO trovare il modo di terminare l'asynctask in modo corretto

    private View1 view1;
    private boolean canPreset=false;

    public ConnectionTask(View1 activity){
        view1=activity;
    }

    @Override
    protected Integer doInBackground(NetworkServiceConnection... params) {

        NetworkServiceConnection networkServiceConnection=params[0];

        while (networkServiceConnection.getService()==null){
        }



        while (networkServiceConnection.getService()==null){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                //TODO gestire
            }
        }

        while (!networkServiceConnection.getService().isSyncComplete())
        {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                //TODO gestire
            }
        }

        canPreset=networkServiceConnection.getService().isCanPreset();

        return null;
    }

    @Override
    protected void onPostExecute(Integer i){
        if(canPreset)
            view1.enableRecordingViewController();
        else
            view1.setRecordingViewControllerUnsupported();
    }
}
