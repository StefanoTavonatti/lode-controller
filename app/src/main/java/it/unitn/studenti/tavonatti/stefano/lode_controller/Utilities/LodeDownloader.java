package it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.LodeResponseHandler;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.NetworkService;
import it.unitn.studenti.tavonatti.stefano.lode_controller.R;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;

/**
 * Created by stefano on 10/09/15.
 */
public class LodeDownloader implements Runnable {

    private String fileType;
    private int nFiles;
    private NetworkService networkService;
    private Context context;

    public LodeDownloader(Context context,NetworkService networkService,String fileType,int nFiles){
        this.fileType=fileType;
        this.nFiles = nFiles;
        this.networkService=networkService;
        this.context=context;
    }

    @Override
    public void run() {//TODO notifica dell'errore

        InetAddress address=null;

        try {
            address=InetAddress.getByName(networkService.getIP());
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return;
        }

        cleanFolder(context,fileType);

        for(int i=1;i<= nFiles;i++){
            downloadFile(address,fileType,context,""+i+".jpg");//TODO cambiare con solo numero appena il server è pronto
        }

        if(fileType.equals(NetworkMessage.SLIDES)){
            networkService.sendCommandToServer(NetworkMessage.GET_PRESET_LIST);
        }
        else {
            /*Brodcast per notificare il download completato, necessario per le operazioni da fare sulla gui*/ //TODO sistemare
            /*Intent intent = new Intent(Constant.NOTIFICATION);
            intent.putExtra(Constant.ACTION, Constant.DOWNLOAD_COMPLETE);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);*/
            LodeResponseHandler.notifyEndOfSync(networkService, context);
        }
    }

    public static void downloadFile(InetAddress address,String fileType,Context context,String file){
        Socket socket= null;
        try {
            socket = new Socket(address,Constant.DOWNLOADER_PORT);//TODO decommentare per stateless
            //socket=NetworkService.socket;
            InputStream inputStream=socket.getInputStream();
            InputStreamReader inputStreamReader=new InputStreamReader(inputStream);
            BufferedReader bufferedReader=new BufferedReader(inputStreamReader);

            PrintWriter out=new PrintWriter(socket.getOutputStream(),true);
            String request="";

            if(fileType.equals(NetworkMessage.SLIDES)){
                request+=NetworkMessage.GET_SLIDE;
            }
            else {
                request+=NetworkMessage.GET_PRESET;
            }

            SettingsDBManager settingsDBManager=new SettingsDBManager(context);
            String tocken=settingsDBManager.readValue(Constant.TOCKEN);

            request+=" "+file+" "+tocken;

            out.println(request);

            //File root=android.os.Environment.getExternalStorageDirectory();
            File root=context.getCacheDir();

            File output=null;

            if(fileType.equals(NetworkMessage.SLIDES)) {
                output = new File(root.getAbsolutePath() + Constant.SLIDE_DIR + "/" + file+".png");//TODO chiedere per png
            }
            else {
                output = new File(root.getAbsolutePath() + Constant.PRESET_DIR + "/" + file+".png");
            }

            FileOutputStream outFile=new FileOutputStream(output);//TODO controllare problema esistenza


            byte[] bytes=new byte[16*1024];//TODO decommentare per stateless

            int count;

            boolean first=true;

            boolean noImage=false;

            while ((count= inputStream.read(bytes))>0){
                if(first){
                    first=false;
                    byte resp[]="NO".getBytes();
                    boolean equals=true;

                    for(int i=0; i<resp.length; i++){
                        if(bytes[i]!=resp[i]){
                            equals=false;
                            break;
                        }
                    }

                    if(equals){
                        Log.d("EQUALS","SI");
                        noImage=true;
                        break;
                    }
                }
                outFile.write(bytes, 0,count);
            }

            if(noImage){//TODO ripulire la cartella quando vado sull'activity
                Bitmap image= BitmapFactory.decodeResource(context.getResources(), R.drawable.no_preset);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                image.compress(Bitmap.CompressFormat.PNG,100,bos);
                byte[] bitmapdata = bos.toByteArray();

                outFile.write(bitmapdata,0,bitmapdata.length);
                outFile.flush();

            }

            outFile.close();

            inputStream.close();
            socket.close();

        } catch (IOException e) {
            Log.d("LodeDownloader","IO Exception");
        }

    }

    public static void cleanFolder(Context context,String fileType){
        //File root=android.os.Environment.getExternalStorageDirectory();
        File root=context.getCacheDir();

        File dir=null;

        if(fileType.equals(NetworkMessage.SLIDES)){
            dir=new File(root.getAbsolutePath()+Constant.SLIDE_DIR);
        }
        else {
            dir=new File(root.getAbsolutePath()+Constant.PRESET_DIR);
        }

        /* non posso cancellare una directory non vuota, quindi cancello dile per file */
        if(dir.exists()){
            if(dir.isDirectory()){
                File[] files=dir.listFiles();

                for(File file:files){
                    file.delete();
                }
            }
            else {
                dir.mkdirs();
            }
        }
        else{
            dir.mkdirs();
        }

        /* in questo modo le slide non saranno visibili nella galleria*/
        File nomedia=new File(dir.getAbsolutePath()+"/.nomedia");

        if(!nomedia.exists()){
            try {
                nomedia.createNewFile();
            } catch (IOException e) {
               Log.d("LodeListener","nomedia error");
            }
        }

    } //TODO messaggio in brodcast fine download e richiesta preset oppure richiesta direttamente da qui

}
