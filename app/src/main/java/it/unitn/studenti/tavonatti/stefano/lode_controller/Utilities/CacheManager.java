package it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;

/**
 * Created by stefano on 06/10/15.
 */
public class CacheManager {

    private Context context;
    private int maxWidth;
    private int maxHeight;

    public CacheManager(Context context,int maxWidth,int maxHeight){
        this.context=context;
        this.maxWidth=maxWidth;
        this.maxHeight=maxHeight;
    }

    private File getFile(String fileName){
        File file=new File(context.getCacheDir()+fileName);

        return file;
    }

    public Bitmap getSlide(String slideName){
        for(int i=0;i<5;i++) {
            File file = getFile(Constant.SLIDE_DIR + "/" + slideName+".png");

            if (file.exists()) {
                Bitmap result=Utilities.scaleBitmapFromFile(file,maxWidth,maxHeight);
                if(result!=null)
                    return result;
                else
                    file.delete();
            }
            Log.d("DOWNLOADER","Redownload file "+slideName+", i="+i);

            InetAddress address;
            SettingsDBManager settingsDBManager=new SettingsDBManager(context);

            try {
                address= InetAddress.getByName(settingsDBManager.readValue(Constant.SERVER_IP));
            } catch (UnknownHostException e) {
                Log.d("BROWSER","Uncknown Host");
                return null;
            }

            LodeDownloader.downloadFile(address, NetworkMessage.SLIDES, context, slideName);//TODO chiedere per png
        }

        return null;
    }

    public Bitmap getPreset(String fileName){
        for(int i=0;i<10;i++) {
            File file = getFile(Constant.PRESET_DIR + "/" + fileName +".png");

            /* se il file non esiste o è impossibile da decodificare rieseguo il download, al max 10 tentativi*/
            if (file.exists()) {
                Bitmap result=Utilities.scaleBitmapFromFile(file,maxWidth,maxHeight);
                if(result!=null)
                    return result;
                else
                    file.delete();
            }
            Log.d("DOWNLOADER","Redownload file "+fileName+", i="+i);

            InetAddress address;
            SettingsDBManager settingsDBManager=new SettingsDBManager(context);

            try {
                address= InetAddress.getByName(settingsDBManager.readValue(Constant.SERVER_IP));
            } catch (UnknownHostException e) {
                Log.d("BROWSER","Uncknown Host");
                return null;
            }

            LodeDownloader.downloadFile(address,NetworkMessage.PRESETS,context,fileName);
        }

        return null;
    }
}
