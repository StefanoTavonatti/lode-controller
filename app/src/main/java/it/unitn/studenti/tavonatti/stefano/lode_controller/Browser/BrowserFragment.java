package it.unitn.studenti.tavonatti.stefano.lode_controller.Browser;

import android.app.Activity;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.File;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.LodeBrodcastReceiver;
import it.unitn.studenti.tavonatti.stefano.lode_controller.R;
import it.unitn.studenti.tavonatti.stefano.lode_controller.SlideBrowserActivity;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;

public class BrowserFragment extends Fragment {

    //TODO listener here e richiamo task
    //TODO pulsanti volume per cambiare le slide
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    public static final String ARG_FILE = "FILE";

    public static final String ARG_ACTIVE = "ACTIVE";

    public static final String ARG_IS_SLIDE="IS_SLIDE";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;//TODO cambiare descrizione

    private View layout;

    private int file;

    private boolean active=false;

    private boolean isSlide;

    private Matrix originalBitmapMatrix=new Matrix();

    private class BrowserBrodcastReceiver extends LodeBrodcastReceiver{

        @Override
        protected void onActiveSlide(){
            Log.d("SLIDE","notification arrived");
            loadBitmap();
        }

        @Override
        protected void onActivePreset(){
            Log.d("PRESET","notification arrived");
            loadBitmap();
        }
    }

    private BrowserBrodcastReceiver receiver=new BrowserBrodcastReceiver();

    public static BrowserFragment create(int pageNumber, int file, boolean active,boolean isSlide){
        BrowserFragment fragment=new BrowserFragment();
        Bundle args=new Bundle();//TODO ripassare
        args.putInt(ARG_PAGE, pageNumber);
        args.putInt(ARG_FILE, file);
        args.putBoolean(ARG_ACTIVE, active);
        args.putBoolean(ARG_IS_SLIDE,isSlide);
        fragment.setArguments(args);
        return fragment;
    }

    public BrowserFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber=getArguments().getInt(ARG_PAGE);//TODO ripassare
        file=getArguments().getInt(ARG_FILE);
        active=getArguments().getBoolean(ARG_ACTIVE);
        isSlide=getArguments().getBoolean(ARG_IS_SLIDE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_browser, container, false);

        //TODO aggiungere qui la creazione del layout

        layout =view;

        loadBitmap();

        ImageView imageView= (ImageView) view.findViewById(R.id.browser_image_view);

        imageView.setOnTouchListener(new ZoomTouchListener(this.getActivity(),originalBitmapMatrix));

        return view;
    }

    public void loadBitmap(){

        //File root=android.os.Environment.getExternalStorageDirectory();
        File root=getActivity().getCacheDir();

        DisplayMetrics metrics=new DisplayMetrics();
        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        /* se il bound del servizio e completo prendo da li la slide attiva*/
        boolean reload=((SlideBrowserActivity) getActivity()).serviceConnection.getService()!=null;

        if(reload) {//TODO verificare correttezza, anchhe del servizio
            if(isSlide)
                active = ((SlideBrowserActivity) getActivity()).serviceConnection.getService().getActiveSlide()==file;
            else
                active = ((SlideBrowserActivity)getActivity()).serviceConnection.getService().getActivePreset()==file;
        }

        String dir="";

        if(isSlide)
            dir=root.getAbsolutePath() + Constant.SLIDE_DIR;
        else
            dir=root.getAbsolutePath() + Constant.PRESET_DIR;

        //(new BitmapLoaderTask(this, metrics.widthPixels, metrics.heightPixels, new File(dir + "/" + file),active)).execute();
        (new BitmapLoaderTask(this, metrics.widthPixels, metrics.heightPixels,""+file,active,isSlide)).execute();//TODO jpg


    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public int getPageNumber(){
        return mPageNumber;
    }

    public void setBitmap(Bitmap img){

        ImageView imageView= (ImageView) layout.findViewById(R.id.browser_image_view);
        imageView.setImageBitmap(img);

        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        originalBitmapMatrix.set(imageView.getImageMatrix());

        ProgressBar spinner= (ProgressBar) layout.findViewById(R.id.browser_progress_bar);
        spinner.setVisibility(View.GONE);
    }

    @Override
    public void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(receiver);
    }

    //TODO notificare cambiamente ad ogni client sulle slide
    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(receiver, new IntentFilter(Constant.NOTIFICATION));//Brodcast locale, solo per la mia app
    //Toast.makeText(context,"onPause",Toast.LENGTH_SHORT).show();
    }

    public int getFile(){
        return file;
    }

}
