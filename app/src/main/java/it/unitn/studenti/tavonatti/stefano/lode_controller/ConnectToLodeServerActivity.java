package it.unitn.studenti.tavonatti.stefano.lode_controller;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.NetworkService;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.Utilities.NetworkServiceConnection;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.NetworkMessage;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Utilities;

public class ConnectToLodeServerActivity extends AppCompatActivity {

    private NetworkServiceConnection serviceConnection=new NetworkServiceConnection();
    private Dialog connectionDialog;
    private boolean dialogBoxOpen=false;

    private Dialog pinDialog;
    private boolean pinDialogOpen=false;

    private final static String ARG_PIN_OPEN="ARG_PIN_OPEN";
    private final static String ARG_CON_OPEN="ARG_CON_OPEN";

    private Context Insance;

    private BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.hasExtra("Result")){
                int result=intent.getIntExtra("Result",Constant.RESULT_FAIL);

                if(result==Constant.RESULT_OK) {
                    hideConnectionDialogBox();
                    showPinDialogBox();
                    //returnResult(result);
                }
                else if(result==Constant.PIN_OK){
                    hideConnectionDialogBox();
                    returnResult(result);
                } else if(result==Constant.PIN_NO){
                    hideConnectionDialogBox();
                    showPinDialogBox();
                    pinDialogWrongCode();
                    Toast.makeText(Insance,"Wrong authorization code",Toast.LENGTH_SHORT).show();
                }
                else if(result==Constant.ERROR){
                    hideConnectionDialogBox();
                    unbindWrapper();
                    Toast.makeText(Insance,"Connection Timeout",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    hideConnectionDialogBox();
                    unbindWrapper();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_to_lode_server);

        Insance=this;

        EditText ipE= (EditText) findViewById(R.id.acitivity2_server_ip);

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

        String ip=settingsDBManager.readValue(Constant.SERVER_IP);

        ipE.setText(ip);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//visualizzare il tasto indietro sull'actionbar
        getSupportActionBar().setHomeButtonEnabled(true);

        if(savedInstanceState!=null){
            pinDialogOpen=savedInstanceState.getBoolean(ARG_PIN_OPEN,false);
            dialogBoxOpen=savedInstanceState.getBoolean(ARG_CON_OPEN,false);
        }

        if(pinDialogOpen){
            showPinDialogBox();
        }

        if(dialogBoxOpen){
            showConnectionDialogBox();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, new IntentFilter(Constant.NOTIFICATION));//Brodcast locale, solo per la mia app
        //registerReceiver(receiver, new IntentFilter(Constant.NOTIFICATION));
        if(Utilities.isMyServiceRunning(NetworkService.class,this.getApplicationContext())){
            Intent intent2=new Intent(this,NetworkService.class);
            this.bindService(intent2, serviceConnection, Context.BIND_AUTO_CREATE);
        }

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

        if(settingsDBManager.readBool(Constant.KEEP_SCREEN_ACTIVE, Constant.DEFAULT_SCREEN_ON))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
       // unregisterReceiver(receiver);
        if(serviceConnection.getService()!=null)
            unbindService(serviceConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_connect_to_lode_server, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id==android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void clickConnect(View view){

        EditText ipField= (EditText) findViewById(R.id.acitivity2_server_ip);

        String str=""+ipField.getText();

        showConnectionDialogBox();
        setConnectionDialogText("Connecting to "+str);

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());
        settingsDBManager.writeRow(Constant.SERVER_IP, str);

        Intent intent=new Intent(this, NetworkService.class);
        intent.setAction(Constant.START_SERVICE);

        intent.putExtra("ServerIP", str);

        String str2=intent.getExtras().getString("ServerIP");

        startService(intent);

        Intent intent2 = new Intent(this, NetworkService.class);
        this.bindService(intent2, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public void showConnectionDialogBox(){
        connectionDialog=new Dialog(this);
        connectionDialog.setContentView(R.layout.connection_dialog_box);

        Button cancel= (Button) connectionDialog.findViewById(R.id.connection_dialog_box_button);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideConnectionDialogBox();//TODO aggiungere anullamento connessione
                unbindWrapper();
            }
        });

        dialogBoxOpen=true;

        connectionDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (dialogBoxOpen) {
                    showConnectionDialogBox();//TODO setText
                }
            }
        });

        connectionDialog.show();
    }


    public void hideConnectionDialogBox(){
        if(dialogBoxOpen){
            dialogBoxOpen=false;
            connectionDialog.dismiss();
        }
    }

    public void setConnectionDialogText(String text){
        if (dialogBoxOpen){
            TextView textView= (TextView) connectionDialog.findViewById(R.id.connection_dialog_box_text);
            textView.setText(text);
        }
    }

    public void returnResult(int result){

        setResult(result);
        /*if(serviceConnection.getService()!=null)
            unbindService(serviceConnection);*/
        finish();
    }

    public void unbindWrapper(){

        if(serviceConnection.getService()!=null) {
            //networkService=null;
            unbindService(serviceConnection);
            serviceConnection.disconnect();
        }

        Intent intent=new Intent(this, NetworkService.class);
        intent.setAction(Constant.STOP_SERVICE);

        startService(intent);
    }

    private void showPinDialogBox(){
        pinDialog=new Dialog(this);
        pinDialog.setContentView(R.layout.pin_dialog_box);

        Button ok= (Button) pinDialog.findViewById(R.id.pin_ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) pinDialog.findViewById(R.id.pin_edit_text);
                String pin = "" + editText.getText();

                ArrayList<String> values = new ArrayList<String>();
                values.add(pin);

                /* chiudo la tastiera che in precedenza ho aperto*/
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(editText.getWindowToken(), 0);

                serviceConnection.getService().sendCommandToServer(NetworkMessage.PIN, values);
                pinDialogOpen = false;
                pinDialog.dismiss();

                showConnectionDialogBox();
            }
        });

        Button cancel= (Button) pinDialog.findViewById(R.id.pin_cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unbindWrapper();

                /* chiudo la tastiera che in precedenza ho aperto*/
                EditText editText= (EditText) pinDialog.findViewById(R.id.pin_edit_text);
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(editText.getWindowToken(), 0);

                pinDialogOpen = false;
                pinDialog.dismiss();
            }
        });

        pinDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (pinDialogOpen)
                    showPinDialogBox();
            }
        });


        pinDialogOpen = true;

        pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                /* apro la tastiera all'apertura della dialogbox*/
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        });

        pinDialog.show();
    }

    private void pinDialogWrongCode(){
        EditText editText= (EditText) pinDialog.findViewById(R.id.pin_edit_text);
        editText.setHintTextColor(Color.RED);
        editText.setHint(getString(R.string.wrong_pin));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        savedInstanceState.putBoolean(ARG_PIN_OPEN,pinDialogOpen);
        savedInstanceState.putBoolean(ARG_CON_OPEN,dialogBoxOpen);
    }
}
