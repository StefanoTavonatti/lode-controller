package it.unitn.studenti.tavonatti.stefano.lode_controller.Network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.NetworkMessage;

/**
 * Created by stefano on 08/09/15.
 */
public class LodeBrodcastReceiver extends BroadcastReceiver {

    public LodeBrodcastReceiver(){
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String action=intent.getStringExtra(Constant.ACTION);

        switch (action) {

            case Constant.ACTION_COMMAND:
                Log.d("REC", "Messaggio ricevuto " + intent.getStringExtra(NetworkMessage.COMMAND_FIELD));
                Log.d("REC", "values " + intent.getStringExtra(NetworkMessage.VALUES_FIELD));
                onActionCommand(intent.getStringExtra(NetworkMessage.COMMAND_FIELD),intent.getStringExtra(NetworkMessage.VALUES_FIELD));
                break;
            case Constant.DOWNLOAD_COMPLETE:
                Log.d("REC","Sync complete");
                onSyncComplete();
                break;
        }
    }

    protected void onActionCommand(String command,String values){
        switch (command){
            case NetworkMessage.ACTIVE_SLIDE:
                onActiveSlide();
                break;
            case NetworkMessage.ACTIVE_PRESET:
                onActivePreset();
                break;
            case NetworkMessage.STATUS://TODO status senza JSON
                try {
                    JSONArray jsonArray=new JSONArray(values);
                    onStatus(jsonArray.getString(0));
                } catch (JSONException e) {
                    Log.d("RECEIVER","Unable to decode JSON");
                }
                break;
            case NetworkMessage.CANTILT:
                onCanTilt(values.equals(NetworkMessage.TRUE));
                break;
            case NetworkMessage.CANPAN:
                onCanPan(values.equals(NetworkMessage.TRUE));
                break;
            case NetworkMessage.CANZOOM:
                onCanZoom(values.equals(NetworkMessage.TRUE));
                break;
        }
    }

    protected void onSyncComplete(){
    }

    protected void onActiveSlide(){}

    protected void onActivePreset(){}

    protected void onStatus(String status){}

    protected void onCanPan(boolean value){}

    protected void onCanTilt(boolean value){}

    protected void onCanZoom(boolean value){}
}
