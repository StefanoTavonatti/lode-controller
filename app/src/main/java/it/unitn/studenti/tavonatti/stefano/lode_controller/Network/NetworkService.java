package it.unitn.studenti.tavonatti.stefano.lode_controller.Network;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.*;
import android.os.Process;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Hashtable;

import it.unitn.studenti.tavonatti.stefano.lode_controller.R;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.NetworkMessage;
import it.unitn.studenti.tavonatti.stefano.lode_controller.View1;

public class NetworkService extends Service {

    private Socket socket=null;
    private Looper serviceLooper;
    private LodeMessageHandler lodeMessageHandler;
    private MyBinder myBinder=new MyBinder();
    private String ip="";
    private Notification notification;
    private boolean connected=false;
    private boolean connectionError=false;
    private String status;
    private boolean syncComplete=false;
    public int slides=0;
    public int presets=4;
    private int activeSlide=-1;
    private int activePreset=-1;
    private InetAddress serverAddress=null;
    private LodeResponseHandler lodeResponseHandler;
    private boolean canPan=false;
    private boolean canTilt=false;
    private boolean canPreset=false;
    private boolean canZoom=false;
    private String tocken="";

    public NetworkService() {
    }

    public boolean isCanPan() {
        return canPan;
    }

    public void setCanPan(boolean canPan) {
        this.canPan = canPan;
    }

    public boolean isCanTilt() {
        return canTilt;
    }

    public void setCanTilt(boolean canTilt) {
        this.canTilt = canTilt;
    }

    public boolean isCanPreset() {
        return canPreset;
    }

    public void setCanPreset(boolean canPreset) {
        this.canPreset = canPreset;
    }

    public boolean isCanZoom() {
        return canZoom;
    }

    public void setCanZoom(boolean canZoom) {
        this.canZoom = canZoom;
    }

    public String getTocken() {
        return tocken;
    }

    public void setTocken(String tocken) {
        this.tocken = tocken;
        SettingsDBManager settingsDBManager= new SettingsDBManager(this.getApplicationContext());
        settingsDBManager.writeRow(Constant.TOCKEN,tocken);//TODO soluzione temporanea per recupera il tocken all'interno della classe che fa il download, cambiare passando il tocken
    }


    public class MyBinder extends Binder {

        public NetworkService getService(){
            return NetworkService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }


    @Override
    public int onStartCommand(Intent intent,int flags,int startid){

        status=this.getString(R.string.state_not_connected);

        if(intent.getAction().equals(Constant.START_SERVICE)){
            Log.i("Service", "Received Start Foreground Intent ");
            notification=new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle(getString(R.string.notification_title))
                    .setContentText("Connecting...")//TODO parametri
                    .setSmallIcon(R.mipmap.feather)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.launcher_icon))
                    .setWhen(System.currentTimeMillis())//TODO verificare
                    .build();

            startForeground(Constant.SERVICE_NOTIFICATION_ID,notification);



            if(intent.hasExtra("ServerIP")){
                ip=intent.getExtras().getString("ServerIP");

                Message msg=lodeMessageHandler.obtainMessage();
                msg.arg1=Constant.CONNECT;
                lodeMessageHandler.sendMessage(msg);
            }



        }else if(intent.getAction().equals(Constant.STOP_SERVICE))
        {
            stopForeground(true);
            stopSelf();
        }

        return START_STICKY;//TODO Voglio che il servizio venga riavviato in caso di chiusura
    }

    public void onCreate(){
        super.onCreate();

        /*creo il thread per il service*/
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_FOREGROUND);
        thread.start();

        serviceLooper=thread.getLooper();
        lodeMessageHandler=new LodeMessageHandler(serviceLooper,this);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //serviceLooper.quitSafely();//TODO valutare di passare ad apilevel18, anche quit termina il lavore prima di chiudere

        /*invio la richiesta di disconessione*/
        Message msg=lodeMessageHandler.obtainMessage();
        msg.arg1=Constant.DISCONNECT;
        lodeMessageHandler.sendMessage(msg);

        /*termina i lavori correnti e poi termina il thread*/
        //serviceLooper.quit();
        Log.i("Service", "Destroy foreground service");
    }

    public void sendCommand(){
        Message msg=lodeMessageHandler.obtainMessage();
        msg.arg1=00;
        lodeMessageHandler.sendMessage(msg);
    }

    public String getIP(){
        return ip;
    }

    public Socket getSocket(){
        return socket;
    }
    
    public void setSocket(Socket socket){
        this.socket=socket;
    }

    public boolean isConnected(){
        return socket!=null;
    }

    public boolean connect(){

        InetAddress address= null;
        try {
            address = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            Toast.makeText(this.getApplicationContext(),"Unknown Host",Toast.LENGTH_SHORT).show();
            connectionError=true;

            /*Notifico l'errore*/
            Intent intent = new Intent(Constant.NOTIFICATION);
            intent.putExtra("Result", Constant.RESULT_FAIL);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

            return false;
        }

        serverAddress=address;

        lodeResponseHandler =new LodeResponseHandler(this.getApplicationContext(),this);

        /*Notifico la conessione*/
        Intent intent = new Intent(Constant.NOTIFICATION);
        intent.putExtra("Result", Constant.RESULT_OK);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        //sendBroadcast(intent);

        /*Aggiorno la notifica nella barra delle notifiche*/
        Intent intent1=new Intent(this, View1.class);
        intent1.putExtra(Constant.IS_CONNECTED_INTENT,true);

        PendingIntent pendingIntent=PendingIntent.getActivity(this,0,intent1,0);


        //                .setContentText("Connected to "+socket.getInetAddress().getHostName())

        notification=new NotificationCompat.Builder(getApplicationContext())
                .setContentTitle(getString(R.string.notification_title))
                .setSmallIcon(R.mipmap.feather)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.launcher_icon))
                .setWhen(System.currentTimeMillis())//TODO verificare
                .setContentIntent(pendingIntent)
                .build();

        NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(Constant.SERVICE_NOTIFICATION_ID,notification);

        status=this.getString(R.string.state_initial);

        //syncWithLodeServer();

        return true;
    }

    public void disconnect(){
        socket=null;
    }

    public boolean isConnectionError(){
        return connectionError;
    }

    public void sendCommandToServer(String command) {//TODO utilizzare looper

        Hashtable<String,String> temp=new Hashtable<String,String>();
        temp.put(NetworkMessage.COMMAND_FIELD, command);

        Message msg=lodeMessageHandler.obtainMessage();
        msg.arg1=Constant.STANDARD_COMMAND;
        msg.obj=temp;

        lodeMessageHandler.sendMessage(msg);
    }

    public boolean sendCommand(String command,String value){
        Socket socket= null;

        try {
            socket = new Socket(serverAddress, Constant.CONTROLLER_PORT);

            SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

            int timeout=settingsDBManager.readInt(Constant.SETTING_TIMEOUT,Constant.DEFAULT_TIMEOUT);

            socket.setSoTimeout(timeout*1000);
        } catch (IOException e) {
            Log.d("NetworkService","connection error");
            lodeResponseHandler.executeCommand(command, "", true);
            return false;
        }

        try {
            PrintWriter out=new PrintWriter(socket.getOutputStream(),true);
            String output;

            if(value!=null){
                output=command+" "+value;
            }
            else{
                output=command;
            }

            output+=" "+tocken;

            out.println(output);
        } catch (IOException e) {
            Toast.makeText(this.getApplicationContext(),"Socket error",Toast.LENGTH_SHORT).show();
            return false;
        }

        try {
            BufferedReader input=new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String response=input.readLine();
            if(response!=null) {
                lodeResponseHandler.executeCommand(command, response,false);
                Log.d("READER_OUT", response);
            }else{
                Log.d("READER OUT","no response");
            }
            socket.close();
            return true;
        } catch (IOException e) {
            Log.d("NetworkService","reader error");
            return false;
        }
    }

    public void sendCommandToServer(String command,ArrayList<String> values) {
        Hashtable<String,String> temp=new Hashtable<String,String>();
        temp.put(NetworkMessage.COMMAND_FIELD, command);
        temp.put(NetworkMessage.VALUES_FIELD,values.get(0));

        Message msg=lodeMessageHandler.obtainMessage();
        msg.arg1=Constant.STANDARD_COMMAND;
        msg.obj=temp;

        lodeMessageHandler.sendMessage(msg);
    }

    //TODO passaggi connessione e deownload iniziati da qui

    public void syncWithLodeServer(){

        sendCommandToServer(NetworkMessage.GET_SESSION_ID);
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status=status;
    }

    public boolean isSyncComplete(){
        return syncComplete;
    }

    public void setSyncComplete(boolean syncComplete){
        this.syncComplete=syncComplete;
    }

    public int getActiveSlide(){
        return activeSlide;
    }

    public int getActivePreset(){
        return activePreset;
    }

    public void setActiveSlide(int activeSlide){
        this.activeSlide=activeSlide;
    }

    public void setActivePreset(int activePreset){
        this.activePreset=activePreset;
    }
}
