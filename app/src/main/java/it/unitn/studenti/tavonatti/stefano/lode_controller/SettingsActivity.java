package it.unitn.studenti.tavonatti.stefano.lode_controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Switch;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;

public class SettingsActivity extends AppCompatActivity {

    private boolean keepOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

        keepOn=settingsDBManager.readBool(Constant.KEEP_SCREEN_ACTIVE,true);

        Switch keepOnSwitch= (Switch) findViewById(R.id.keep_on_switch);
        keepOnSwitch.setChecked(keepOn);

        EditText timeoutEditText= (EditText) findViewById(R.id.setting_connection_timemout);
        timeoutEditText.setText(""+settingsDBManager.readInt(Constant.SETTING_TIMEOUT,Constant.DEFAULT_TIMEOUT));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//visualizzare il tasto indietro sull'actionbar
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_check_white_24dp);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

        if(settingsDBManager.readBool(Constant.KEEP_SCREEN_ACTIVE,Constant.DEFAULT_SCREEN_ON))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void onKeepOnChanged(View view){
        Switch keep= (Switch) view;

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

        settingsDBManager.writeBool(Constant.KEEP_SCREEN_ACTIVE, keep.isChecked());
    }

    @Override
    public void onPause(){
        super.onPause();

        EditText timeoutEditText= (EditText) findViewById(R.id.setting_connection_timemout);

        try {
            int value = Integer.parseInt("" + timeoutEditText.getText());
            SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());
            settingsDBManager.writeInt(Constant.SETTING_TIMEOUT,value);
        }catch(NumberFormatException n){

        }
    }
}
