package it.unitn.studenti.tavonatti.stefano.lode_controller.Browser;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.AsyncTask;

import it.unitn.studenti.tavonatti.stefano.lode_controller.R;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.CacheManager;

/**
 * Created by stefano on 19/09/15.
 */
public class BitmapLoaderTask extends AsyncTask<Void,Void,Bitmap>{

    private BrowserFragment browserFragment;
    private int maxWidth;
    private int maxHeight;
    private String file;
    private boolean active=false;
    private boolean isSlide;

    public BitmapLoaderTask(BrowserFragment slideBrowserActivity, int maxWidth,int maxHeight,String file,boolean active,boolean isSlide){
        super();

        this.browserFragment =slideBrowserActivity;
        this.maxWidth=maxWidth;
        this.maxHeight=maxHeight;
        this.file=file;
        this.active=active;
        this.isSlide=isSlide;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        CacheManager cacheManager=new CacheManager(browserFragment.getActivity(),maxWidth,maxHeight);

        Bitmap result=null;

        if(isSlide)
            result=cacheManager.getSlide(file);
        else
            result=cacheManager.getPreset(file);


        //Bitmap result= Utilities.scaleBitmapFromFile(file,maxWidth,maxHeight);

        if(result==null)
        {
            result= BitmapFactory.decodeResource(browserFragment.getResources(), R.mipmap.ic_launcher);
        }

       if(active){ //TODO passare active slide

            android.graphics.Bitmap.Config bitmapConfig=result.getConfig();
            if(bitmapConfig==null){
                bitmapConfig= Bitmap.Config.ARGB_8888;
            }

           result=result.copy(bitmapConfig,true);

           Canvas canvas=new Canvas(result);
           Paint paint=new Paint(Paint.ANTI_ALIAS_FLAG);

           paint.setColor(Color.rgb(72, 255, 91));
           paint.setStrokeWidth((Math.max(result.getWidth(),result.getHeight())/20));//TODO inserire l'immagine nello sfondo
           paint.setStyle(Paint.Style.STROKE);

           canvas.drawRect(new RectF(0,0,result.getWidth(),result.getHeight()),paint);


        }

        return result;
    }

    @Override
    protected void onPostExecute(Bitmap img){
        browserFragment.setBitmap(img);
    }
}
