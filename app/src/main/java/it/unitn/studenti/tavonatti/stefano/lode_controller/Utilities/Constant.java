package it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities;

/**
 * Created by stefano on 21/08/15.
 */
public class Constant {

    public final static int CONTROLLER_PORT=2147;
    public final static int DOWNLOADER_PORT=2147;

    public final static String START_SERVICE="START";
    public final static String STOP_SERVICE="STOP";

    public final static int SERVICE_NOTIFICATION_ID=1;

    public final static int CONNECT=0;
    public final static int DISCONNECT=1;
    public final static int STANDARD_COMMAND=3;

    public final static int CHANGE_RECORDING_STATE=2;

    public final static int RESULT_OK=200;
    public final static int RESULT_FAIL=201;

    public final static int PIN_OK=300;
    public final static int PIN_NO=301;

    public final static int ERROR=400;

    public final static String SERVER_IP="ServerIP";

    public final static String IS_CONNECTED_INTENT="IsConnectedIntent";

    public final static String NOTIFICATION="stefano.tavonatti.studenti.unitn.it.lode_controller.Utilities.Constant.NOTIFICATION";

    public final static String DOWNLOADER_NOTIFICATION="stefano.tavonatti.studenti.unitn.it.lode_controller.Utilities.DOWNLOADER_NOTIFICATION";

    public final static String OLD_SESSION_ID="OLD_SESSION_ID";

    public final static String LODE_DIR="/LodeFiles";

    public final static String SLIDE_DIR=LODE_DIR+"/Slides";

    public final static String PRESET_DIR=LODE_DIR+"/Presets";

    public final static String DOWNLOAD_COMPLETE="DOWNLOAD_COMPLETE";

    public final static String ACTION="Action";

    public final static String FILETYPE="FileType";

    public final static String ACTION_COMMAND="ActionCommand";

    public final static String STATUS="STATUS";

    public final static String SYNC_COMPLETE="SYNC_COMPLETE";

    public final static String ACTIVE_SLIDE="ACTIVE_SLIDE";

    /* SETTINGS */

    public final static String KEEP_SCREEN_ACTIVE="KEEP_SCREEN_ACTIVE";

    public final static boolean DEFAULT_SCREEN_ON=true;

    public final static String TOCKEN="TOKEN";

    public final static String SETTING_TIMEOUT="TIMEOUT";

    public final static int DEFAULT_TIMEOUT=15;

}
