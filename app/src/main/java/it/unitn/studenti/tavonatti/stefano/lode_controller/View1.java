package it.unitn.studenti.tavonatti.stefano.lode_controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.AsyncTasks.ConnectionTask;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.LodeBrodcastReceiver;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.NetworkService;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.Utilities.NetworkServiceConnection;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.LodeDownloader;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.NetworkMessage;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Utilities;

public class View1 extends AppCompatActivity {

    public boolean connected=false;
    private NetworkServiceConnection serviceConnection=new NetworkServiceConnection();

    private class View1BrodcastReceiver extends LodeBrodcastReceiver{

        @Override
        protected void onStatus(String status){
            setState(status);
        }
    }

    private BroadcastReceiver receiver=new View1BrodcastReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view1);

        /*controllo se è stato salvato lo stato attuale nel bundle*/
        if(savedInstanceState!=null){
            connected=savedInstanceState.getBoolean("IsConnected",false);
        }

        /*controllo se l'activity viene avviata da un intent contenente lo stato attuale*/
        Intent intent=getIntent();

        if(intent!=null){
            if(intent.hasExtra(Constant.IS_CONNECTED_INTENT)){
                connected=intent.getBooleanExtra(Constant.IS_CONNECTED_INTENT,false);
            }
        }

        if(Utilities.isMyServiceRunning(NetworkService.class,getApplicationContext())){
            connected=true;//TODO controllare
        }

        if(connected){
            setOnline();
        }
        else {
            setOffline();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(this,SettingsActivity.class);
            startActivity(intent);
            return true;
        }else if(id==R.id.refresh_view1) {
            if(serviceConnection.getService()!=null){
                serviceConnection.getService().sendCommandToServer(NetworkMessage.GET_SESSION_ID); //TODO fare una procedura che non richieda del session id
            }
        }/*else if(id==R.id.test_view1){//TODO cancellare
            onClickTest(null);
        }*/

        return super.onOptionsItemSelected(item);
    }

    public void onSaveInstanceState(Bundle savedInstanceState){

        savedInstanceState.putBoolean("IsConnected",connected);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onPause(){
        super.onPause();
        if(connected){
            unbindService(serviceConnection);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(connected)
        {
            Intent intent2=new Intent(this,NetworkService.class);
            this.bindService(intent2, serviceConnection, Context.BIND_AUTO_CREATE);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, new IntentFilter(Constant.NOTIFICATION));//Brodcast locale, solo per la mia app

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());


        if(settingsDBManager.readBool(Constant.KEEP_SCREEN_ACTIVE,Constant.DEFAULT_SCREEN_ON))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    public void onClickConnect(View view){
        if(!connected) {
            Intent intent = new Intent(this, ConnectToLodeServerActivity.class);
            this.startActivityForResult(intent,Constant.CONNECT);
        }else{
            connected=false;
            if(serviceConnection.getService()!=null) {
                //networkService=null;
                unbindService(serviceConnection);
                serviceConnection.disconnect();
            }
            
            Intent intent=new Intent(this, NetworkService.class);
            intent.setAction(Constant.STOP_SERVICE);

            startService(intent);

            setOffline();
            Toast.makeText(this.getApplicationContext(), "Disconnected from the host", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickChangeRecordingState(View view){
        Intent intent=new Intent(this,ChangeRecordingStateActivity.class);
        startActivityForResult(intent, Constant.CHANGE_RECORDING_STATE);
    }

    public void onClickTest(View view){
        //serviceConnection.getService().sendCommandToServer(NetworkMessage.GET_SLIDE_LIST);
        /*File root=android.os.Environment.getExternalStorageDirectory();
        File dir=new File(root.getAbsolutePath()+"/lodeFile");
        Toast.makeText(this,dir.toString(),Toast.LENGTH_SHORT).show();
        dir.mkdirs();*/
        //Toast.makeText(this,"running: "+ Utilities.isMyServiceRunning(NetworkService.class,this.getApplicationContext()),Toast.LENGTH_SHORT).show();
        /*getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);*/
        Toast.makeText(this,""+getCacheDir().getAbsolutePath(),Toast.LENGTH_SHORT).show();
    }

    public void onClickSlideControl(View view){
        Intent intent=new Intent(this,SlideControlActivity.class);
        intent.putExtra(Constant.SYNC_COMPLETE,serviceConnection.getService().isSyncComplete());//TODO click prima del bind
        intent.putExtra(Constant.ACTIVE_SLIDE,serviceConnection.getService().getActiveSlide());
        startActivity(intent);

        Log.d("BACK", "sync: " + serviceConnection.getService().isSyncComplete());
    }

    public void setOnline(){
        Button changeRecordState= (Button) findViewById(R.id.activity1_change_record_state);
        Button slideController= (Button) findViewById(R.id.activity1_slide_controller);
        Button recorder= (Button) findViewById(R.id.activity1_recording);
        Button connect= (Button) findViewById(R.id.activity1_connect);
        Button cameraControll= (Button) findViewById(R.id.camera_controll_view1);

        cameraControll.setEnabled(true);
        changeRecordState.setEnabled(true);
        slideController.setEnabled(true);
        //recorder.setEnabled(true);
        recorder.setText(getString(R.string.waiting));

        connect.setText(getString(R.string.disconnect_lode_server));

        Intent intent2=new Intent(this,NetworkService.class);
        this.bindService(intent2, serviceConnection, Context.BIND_AUTO_CREATE);

        TextView textView= (TextView) this.findViewById(R.id.state_activity1);
        textView.setText(getString(R.string.state_initial));

        (new ConnectionTask(this)).execute(serviceConnection);
    }

    public void setOffline(){

        Button changeRecordState= (Button) findViewById(R.id.activity1_change_record_state);
        Button slideController= (Button) findViewById(R.id.activity1_slide_controller);
        Button recorder= (Button) findViewById(R.id.activity1_recording);
        Button connect= (Button) findViewById(R.id.activity1_connect);
        Button cameraControll= (Button) findViewById(R.id.camera_controll_view1);

        cameraControll.setEnabled(false);
        changeRecordState.setEnabled(false);
        slideController.setEnabled(false);
        recorder.setEnabled(false);//TODO false

        connect.setText(getString(R.string.connect_to_lode_server));

        TextView textView= (TextView) findViewById(R.id.state_activity1);
        textView.setText(getString(R.string.state_not_connected));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==Constant.CONNECT){
            if(resultCode==Constant.PIN_OK){
                connected=true;
                setOnline();
            }
        }
        else if(requestCode==Constant.DISCONNECT){
            if(resultCode==Constant.RESULT_OK){
                connected=false;
                setOffline();
            }
        }
        else if(requestCode==Constant.CHANGE_RECORDING_STATE && data!=null){
            String status=data.getStringExtra(Constant.STATUS);

            TextView textView= (TextView) findViewById(R.id.state_activity1);//TODO controllare come devo impostare
                                                                             //TODO brodcast messaggio cambio stato

            textView.setText(status);
        }
    }

    public void onClickRecordingViewController(View view){
        LodeDownloader.cleanFolder(this,NetworkMessage.PRESETS);
        Intent intent=new Intent(this,SlideBrowserActivity.class);
        intent.putExtra(NetworkMessage.ACTIVE_SLIDE,serviceConnection.getService().getActivePreset());
        intent.putExtra(NetworkMessage.PRESETS,serviceConnection.getService().presets);
        startActivity(intent);
    }

    public void enableRecordingViewController(){//TODO mettere anche aggiornamento messaggio, scritta downloading e movimento puntini
        Button recorder= (Button) findViewById(R.id.activity1_recording);
        recorder.setText(getString(R.string.recording_view_control));
        recorder.setEnabled(true);

        if(serviceConnection.getService()!=null){
            setState(serviceConnection.getService().getStatus());
        }
    }

    public void setRecordingViewControllerUnsupported(){
        Button recorder= (Button) findViewById(R.id.activity1_recording);
        recorder.setEnabled(false);
        recorder.setText(getString(R.string.unsupported));
    }

    /*@Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if(serviceConnection.getService()!=null){
                    serviceConnection.getService().sendCommandToServer(NetworkMessage.NEXT);
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if(serviceConnection.getService()!=null){
                    serviceConnection.getService().sendCommandToServer(NetworkMessage.PREVIOUS);
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }*/

    private void setState(String state){
        TextView textView= (TextView) findViewById(R.id.state_activity1);
        textView.setText(state);
    }

    public void onClickCameraControll(View view){
        if(serviceConnection.getService()==null)
            return;

        Intent intent=new Intent(this,CameraControl.class);

        intent.putExtra(CameraControl.ARG_CANPAN,serviceConnection.getService().isCanPan());
        intent.putExtra(CameraControl.ARG_CANTILT,serviceConnection.getService().isCanTilt());
        intent.putExtra(CameraControl.ARG_CANZOOM,serviceConnection.getService().isCanZoom());

        startActivity(intent);
    }
}
