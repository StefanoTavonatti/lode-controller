package it.unitn.studenti.tavonatti.stefano.lode_controller.Network.Utilities;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.NetworkService;

/**
 * Created by stefano on 22/08/15.
 */
public class NetworkServiceConnection implements ServiceConnection {

    private NetworkService networkService;

    public NetworkServiceConnection(){
        super();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d("Service conn", "Connection created");
        NetworkService.MyBinder myBinder = (NetworkService.MyBinder) service;
        networkService=myBinder.getService();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        networkService=null;
    }

    public NetworkService getService(){
        return networkService;
    }

    public void disconnect(){
        networkService=null;
    }
}
