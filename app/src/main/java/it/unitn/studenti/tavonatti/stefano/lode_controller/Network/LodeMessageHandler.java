package it.unitn.studenti.tavonatti.stefano.lode_controller.Network;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.util.Hashtable;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.NetworkMessage;

/**
 * Created by stefano on 21/08/15.
 */
public class LodeMessageHandler extends Handler{

    private NetworkService networkService;

    public LodeMessageHandler(Looper looper,NetworkService networkService){
        super(looper);
        this.networkService=networkService;
    }

    @Override
    public void handleMessage(Message msg){

        switch (msg.arg1){
            case 0:
                if(!networkService.isConnected()){
                    networkService.connect();
                }
                break;
            case 1:
                if(networkService.isConnected()){
                    networkService.disconnect();

                    /*termina il thread*/
                    this.getLooper().quit();
                }
                break;
            case 3:
                if(!(msg.obj instanceof Hashtable)){
                    Log.d("MessageHandler","Message error");
                    break;
                }
                Hashtable<String,String> cmd= (Hashtable<String, String>) msg.obj;
                String command=cmd.get(NetworkMessage.COMMAND_FIELD);
                String value=cmd.get(NetworkMessage.VALUES_FIELD);

                boolean send_error=!networkService.sendCommand(command,value);

                break;
        }
    }
}
