package it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by stefano on 24/08/15.
 */
public class SettingsDBManager extends SQLiteOpenHelper {
    private static final String TABLE_NAME="settings";
    private static final int DB_VERSION =1;
    private static final String COLUMN_SETTING="setting";
    private static final String COLUMN_VALUE="valore";
    private static final String SQL_CREATE_TABLE="CREATE TABLE "+TABLE_NAME+" ("
            +COLUMN_SETTING+" TEXT PRIMARY KEY NOT NULL,"
            +COLUMN_VALUE+" TEXT)"
            ;
    private static final String SQL_DELETE_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    private static final String TRUE_VALUE="TRUE";
    private static final String FALSE_VALUE="FALSE";

    public SettingsDBManager(Context context) {
        super(context, TABLE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //CANCELLO LA VECCHIA TABELLA E NE CREO UNA NUOVA
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }

    public boolean writeRow(String setting, String value){

        ContentValues values = new ContentValues();


        if(this.readValue(setting).equals("")) {

            SQLiteDatabase db = this.getWritableDatabase();

            values.put(this.COLUMN_SETTING, setting);
            values.put(this.COLUMN_VALUE, value);


            long newRow = db.insert(this.TABLE_NAME, null, values);
            db.close();

            return newRow > -1;
        }
        else{
            SQLiteDatabase db=this.getReadableDatabase();

            values.put(this.COLUMN_VALUE, value);

            long rowNum=db.update(this.TABLE_NAME,values,this.COLUMN_SETTING+"=\""+setting+"\"",null);
            db.close();

            return rowNum>-1;
        }
    }

    public String readValue(String setting){
        String result="";
        SQLiteDatabase db=this.getReadableDatabase();

        String columns[]=new String[]{COLUMN_VALUE};

        Cursor c=db.query(true,TABLE_NAME,columns,COLUMN_SETTING+"=\""+setting+"\"",null,null,null,COLUMN_SETTING+" ASC",null);

        if(c.getCount()>0) {
            c.moveToFirst();
            result = c.getString(0);
        }

        db.close();

        return result;
    }

    public boolean readBool(String setting){
        String str=readValue(setting);

        return str.equals(TRUE_VALUE);
    }

    public boolean readBool(String setting,boolean defaultValue){
        String str=readValue(setting);

        if(str.equals("")){
            if(writeBool(setting,defaultValue))
                return defaultValue;
            else
                return false;
        }

        return str.equals(TRUE_VALUE);
    }

    public boolean writeBool(String setting,boolean value){

        if(value)
            return writeRow(setting,TRUE_VALUE);
        else
            return writeRow(setting,FALSE_VALUE);
    }

    public boolean writeInt(String setting,int value){
        return writeRow(setting,""+value);
    }

    public int readInt(String setting, int defaultValue){
        String str=readValue(setting);

        if(str.equals("")){
            writeInt(setting,defaultValue);
            return  defaultValue;
        }
        else {
            int value;

            try {
                value = Integer.parseInt(str);
                return value;
            }catch (NumberFormatException e){
                writeInt(setting,defaultValue);
                return defaultValue;
            }
        }

    }

}
