package it.unitn.studenti.tavonatti.stefano.lode_controller.Browser;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by stefano on 26/09/15.
 */
public class ZoomableViewPager extends ViewPager{

    //TODO zommo l'imageView e quando è in zoom setto questo a false, oppure quando esco dall'image view
    /**
     * serve per controllare che l'immagine sia zoommata
     */
    public static boolean onZoom=false;
    private Context context;

    public ZoomableViewPager(Context context, AttributeSet attrs) {//Costruttore con attributi per chiamarlo direttamente dall xml
        super(context, attrs);
        this.context=context;
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent){
        boolean superResult=super.onTouchEvent(motionEvent);


        ((Activity)context).getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

        return superResult;
    }

    @Override
    /**
     * ritornare false per non permettere lo swip, true altrimenti
     */
    public boolean onInterceptTouchEvent(MotionEvent event) {//TODO i doppi tocchi vengono gia inviati

        //return !onZoom;
        //return false; /* con true l'evento non viene propagato e si puo effettura lo swi*/

        if(!onZoom)
            return super.onInterceptTouchEvent(event);
        else
            return false;
    }


}
