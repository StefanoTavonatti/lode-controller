package it.unitn.studenti.tavonatti.stefano.lode_controller.Network;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.net.Socket;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.LodeDownloader;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.NetworkMessage;

/**
 * Created by stefano on 07/09/15.
 */
public class LodeResponseHandler {

    private Socket socket;
    private Context context;
    private NetworkService networkService;
    private boolean newSession=false;


    public LodeResponseHandler(Socket socket, Context context, NetworkService networkService){
        this.socket=socket;
        this.context=context;
        this.networkService=networkService;
    }

    public LodeResponseHandler(Context context, NetworkService networkService){
        this.context=context;
        this.networkService=networkService;
    }

    public void executeCommand(String command,String val,boolean error) {
        switch (command) {
            case NetworkMessage.PIN:
                if(error){
                    Intent intent_2 = new Intent(Constant.NOTIFICATION);
                    intent_2.putExtra("Result", Constant.ERROR);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent_2);
                    break;
                }

                if(val!=null){
                    if(!val.equals(NetworkMessage.NO)){

                        networkService.setTocken(val);

                        networkService.sendCommandToServer(NetworkMessage.CANPAN);
                        networkService.sendCommandToServer(NetworkMessage.CANTILT);
                        networkService.sendCommandToServer(NetworkMessage.CANZOOM);
                        networkService.sendCommandToServer(NetworkMessage.GET_SLIDE_LIST);
                        networkService.sendCommandToServer(NetworkMessage.CANPRESET);

                        LodeDownloader.cleanFolder(context, NetworkMessage.SLIDES);
                        LodeDownloader.cleanFolder(context,NetworkMessage.PRESETS);

                        Intent intent_ = new Intent(Constant.NOTIFICATION);
                        intent_.putExtra("Result", Constant.PIN_OK);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent_);

                    }
                    else {
                        Intent intent_2 = new Intent(Constant.NOTIFICATION);
                        intent_2.putExtra("Result", Constant.PIN_NO);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent_2);
                    }

                    break;
                }

            case NetworkMessage.GET_SLIDE_LIST:
                if (val != null) {

                    int intVal = 0;

                    try {
                        intVal = Integer.valueOf(val);
                    } catch (NumberFormatException e) {
                        Log.d("Listener", "Unable to decode message");
                        break;
                    }

                    networkService.slides = intVal;
                    Log.d("Listener", "SLIDE NUMBER " + intVal);

                }

                break;

            case NetworkMessage.GET_PRESET_LIST:
                if (val == null)
                    break;

                int intVal = 0;

                try {
                    intVal = Integer.valueOf(val);
                } catch (NumberFormatException e) {
                    Log.d("Listener", "Unable to decode message");
                    break;
                }

                networkService.presets = intVal;
                Log.d("Listener", "PRESET NUMBER " + intVal);


                notifyEndOfSync(networkService,context);//TODO rimuovere

                break;

            case NetworkMessage.GET_STATUS:
                networkService.setStatus(val);
                Log.d("STATUS", val);
                sendLocalBrodcastNotification(NetworkMessage.STATUS, val);
                break;

            case NetworkMessage.GET_ACTIVE_SLIDE:
                if (val == null)
                    break;

                int intVal1 = 0;

                try {
                    ;
                    intVal1 = Integer.valueOf(val);
                } catch (NumberFormatException e) {
                    Log.d("Listener", "Unable to decode message");
                    break;
                }
                networkService.setActiveSlide(intVal1);
                sendLocalBrodcastNotification(NetworkMessage.ACTIVE_SLIDE, val);
                break;

            case NetworkMessage.GET_ACTIVE_PRESET:
                if (val == null)
                    break;

                int intVal2 = 0;

                try {
                    intVal2 = Integer.valueOf(val);
                } catch (NumberFormatException e) {
                    Log.d("Listener", "Unable to decode message");
                    break;
                }
                networkService.setActivePreset(intVal2);
                sendLocalBrodcastNotification(NetworkMessage.ACTIVE_PRESET, val);
                break;

            case NetworkMessage.NEXT: //TODO da fare per recording pause stop
            case NetworkMessage.PREVIOUS:
            case NetworkMessage.SET_SLIDE:
                if(val==null)
                    break;

                try
                {
                    int slide=Integer.valueOf(val).intValue();
                    networkService.setActiveSlide(slide);
                }catch (NumberFormatException e){
                    networkService.setActiveSlide(-1);
                }

                sendLocalBrodcastNotification(NetworkMessage.ACTIVE_SLIDE, val);
                break;

            case NetworkMessage.CANPAN:
                networkService.setCanPan(val.equals(NetworkMessage.TRUE));
                sendLocalBrodcastNotification(NetworkMessage.CANPAN, val);
                break;

            case NetworkMessage.CANTILT:
                networkService.setCanTilt(val.equals(NetworkMessage.TRUE));
                sendLocalBrodcastNotification(NetworkMessage.CANTILT, val);
                break;

            case NetworkMessage.CANPRESET:
                boolean canPreset=val.equals(NetworkMessage.TRUE);
                networkService.setCanPreset(canPreset);
                if(canPreset)
                    networkService.sendCommandToServer(NetworkMessage.GET_PRESET_LIST);
                else
                    notifyEndOfSync(networkService,context);//TODO rimuovere

                break;

            case NetworkMessage.CANZOOM:
                boolean canZoom=val.equals(NetworkMessage.TRUE);
                networkService.setCanZoom(canZoom);
                sendLocalBrodcastNotification(NetworkMessage.CANZOOM, val);
                break;

            default:
                Intent intent = new Intent(Constant.NOTIFICATION);
                intent.putExtra(Constant.ACTION, Constant.ACTION_COMMAND);
                intent.putExtra(NetworkMessage.COMMAND_FIELD, command);
                intent.putExtra(NetworkMessage.VALUES_FIELD, val);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                break;

        }
    }

    public static void notifyEndOfSync(NetworkService networkService,Context context){
        Intent intent=new Intent(Constant.NOTIFICATION);
        intent.putExtra(Constant.ACTION,Constant.DOWNLOAD_COMPLETE);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        networkService.setSyncComplete(true);
    }

    private void sendLocalBrodcastNotification(String command,String values){
        Intent intent = new Intent(Constant.NOTIFICATION);
        intent.putExtra(Constant.ACTION,Constant.ACTION_COMMAND);
        intent.putExtra(NetworkMessage.COMMAND_FIELD, command);
        intent.putExtra(NetworkMessage.VALUES_FIELD,values);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
