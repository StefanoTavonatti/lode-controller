package it.unitn.studenti.tavonatti.stefano.lode_controller;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;

import it.unitn.studenti.tavonatti.stefano.lode_controller.Browser.BrowserFragment;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.NetworkService;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Network.Utilities.NetworkServiceConnection;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.Constant;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.DBManager.SettingsDBManager;
import it.unitn.studenti.tavonatti.stefano.lode_controller.Utilities.NetworkMessage;

public class SlideBrowserActivity extends AppCompatActivity {


    /**
     * chiave per cercare l'elemento corrente nel bundle
     */
    private final static String ARG_CURRENT_ITEM="ARG_CURRENT_ITEM";

    /**
     * elemento corrente nel pager
     */
    private int currentItem=-1;

    /**
     * The number of pages (wizard steps) to show in this demo.//TODO cambiare con commento in italiano
     */
    private static  int NUM_PAGES = 6;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;
    private int activeSlide=-1;

    private Integer slides=null;

    private Integer presets=null;

    public NetworkServiceConnection serviceConnection=new NetworkServiceConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_browser);

        if(getIntent().hasExtra(NetworkMessage.SLIDES)) {
            slides=Integer.valueOf(getIntent().getIntExtra(NetworkMessage.SLIDES, 0));
            NUM_PAGES=slides;
        }
        else if(getIntent().hasExtra(NetworkMessage.PRESETS)){
            presets=Integer.valueOf(getIntent().getIntExtra(NetworkMessage.PRESETS,0));
            NUM_PAGES=presets;
        }

        int i=NUM_PAGES;

        //TODO usare il get sull'intent per capire se slide o preset

        if(getIntent().hasExtra(NetworkMessage.ACTIVE_SLIDE)){
            activeSlide=getIntent().getIntExtra(NetworkMessage.ACTIVE_SLIDE, -1);
        }

        if(savedInstanceState!=null){
            activeSlide=savedInstanceState.getInt(NetworkMessage.ACTIVE_SLIDE);
            currentItem=savedInstanceState.getInt(ARG_CURRENT_ITEM);
        }

        mPager= (ViewPager) findViewById(R.id.pager);
        mPagerAdapter=new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When changing pages, reset the action bar actions since they are dependent
                // on which page is currently active. An alternative approach is to have each
                // fragment expose actions itself (rather than the activity exposing actions),
                // but for simplicity, the activity provides the actions in this sample.
                invalidateOptionsMenu();
            }
        });
        //getSupportActionBar().hide();
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);//visualizzare il tasto indietro sull'actionbar
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        
        mPager.setCurrentItem(getActiveID());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_slide_browser, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {//TODO rotazione schermo slide attiva
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(this,SettingsActivity.class);
            startActivity(intent);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause(){
        super.onPause();

        if(serviceConnection.getService()!=null){
            if(slides!=null)
                activeSlide=serviceConnection.getService().getActiveSlide();
            else
            {
                activeSlide=serviceConnection.getService().getActivePreset();
            }
        }

        unbindService(serviceConnection);
    }

    @Override
    public void onResume(){
        super.onResume();

        Intent intent2=new Intent(this,NetworkService.class);
        this.bindService(intent2, serviceConnection, Context.BIND_AUTO_CREATE);

        if(currentItem!=-1)
            mPager.setCurrentItem(currentItem);

        SettingsDBManager settingsDBManager=new SettingsDBManager(this.getApplicationContext());

        if(settingsDBManager.readBool(Constant.KEEP_SCREEN_ACTIVE,Constant.DEFAULT_SCREEN_ON))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    @Override
    public void onSaveInstanceState(Bundle onSaveInstanceState){

        onSaveInstanceState.putInt(NetworkMessage.ACTIVE_SLIDE, activeSlide);
        onSaveInstanceState.putInt(ARG_CURRENT_ITEM,mPager.getCurrentItem());
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /* lo slider genere la pagina dopo o prima dipende dalla porte in cui si scorre*/
        @Override
        public Fragment getItem(int position) {
            Log.d("BROWSER", "Requesting page num " + position);

            boolean active= (position+1)==activeSlide;

            return BrowserFragment.create(position,position+1,active,slides!=null);//TODO gestire errore
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public void onClickBackSlideBrowser(View view){//TODO back e set in stile material, rotondi sopra alla slide
        onBackPressed();
    }

    public void onClickSet(View view){
        //BrowserFragment fragment= (BrowserFragment) ((ScreenSlidePagerAdapter) mPagerAdapter).getItem(mPager.getCurrentItem());
        String file="";
        ArrayList<String> values=new ArrayList<String>();
        String command="";

        if(slides!=null)
        {
            Toast.makeText(this,"Active Slide:"+(mPager.getCurrentItem()+1),Toast.LENGTH_SHORT).show();

            file=""+(mPager.getCurrentItem()+1);
            command=NetworkMessage.SET_SLIDE;
        }
        else {
            Toast.makeText(this,"Active Preset:"+(mPager.getCurrentItem()+1),Toast.LENGTH_SHORT).show();

            file=""+(mPager.getCurrentItem()+1);
            command=NetworkMessage.SET_PRESET;
        }

        values.add(file);
        serviceConnection.getService().sendCommandToServer(command,values);
    }

    public int getActiveID(){
        return activeSlide-1;
    }

}
